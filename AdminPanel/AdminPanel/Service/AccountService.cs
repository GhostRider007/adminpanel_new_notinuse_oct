﻿using AdminPanel.Helper;
using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminPanel.Service
{
    public static class AccountService
    {
        public static LedgerModel GetLedgerDetails(string userType, string loginID, LedgerModel model)
        {            
            if (!string.IsNullOrEmpty(model.FilterFromDate))
            {
                model.FilterFromDate = model.FilterFromDate + " 12:00:00 AM";
            }
            else
            {
                DateTime dtNow = DateTime.Now;
                model.FilterFromDate = dtNow.Year.ToString() + "-" + dtNow.Month.ToString() + "-" + dtNow.Day.ToString() + " 12:00:00 AM";
            }

            if (!string.IsNullOrEmpty(model.FilterToDate))
            {
                model.FilterToDate = model.FilterToDate + " 11:59:59 PM";
            }
            return AccountHelper.GetLedgerDetails(userType, loginID, model);
        }
        public static Dictionary<string, string> GetBookingTypeFromLedger()
        {
            return AccountHelper.GetBookingTypeFromLedger();
        }
        public static Dictionary<string, string> GetTransTypeFromLedger()
        {
            return AccountHelper.GetTransTypeFromLedger();
        }
    }
}