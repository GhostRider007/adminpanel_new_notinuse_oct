﻿using AdminPanel.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static AdminPanel.Models.ProfileModel;

namespace AdminPanel.Service
{
    public static class ProfileService
    {
        #region[Change Password]
        internal static bool ChangePassword(string userid, string oldpassword, string newpassword)
        {
           return ProfileHelper.ChangePassword(userid, oldpassword, newpassword);
        }
        #endregion

        #region[AgencyTDS]
        internal static string AgencyList_ddl()
        {
            return ProfileHelper.AgencyList_ddl();
        }
        internal static Agency GetAgency(string userid)
        {
            return ProfileHelper.GetAgency(userid);
        }
        internal static bool UpdateAgencyTDS(int id, string tds)
        {
            return ProfileHelper.UpdateAgencyTDS(id, tds);
        }
        #endregion
    }
}