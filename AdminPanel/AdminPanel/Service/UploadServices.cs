﻿using AdminPanel.Helper;
using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static AdminPanel.Models.UploadModel;

namespace AdminPanel.Service
{
    public class UploadServices
    {
        #region[Bank Details]
        internal static List<BankDetails> GetBankDetails()
        {
            return UploadHelper.GetBankDetails();
        }
        internal static bool DeleteBankDetails(string id)
        {
            return UploadHelper.DeleteBankDetails(id);
        }
        internal static bool AddBankDetails(BankDetails model)
        {
            return UploadHelper.AddBankDetails(model);
        }
        #endregion

        #region[Agency Credit And Debit]
        internal static string AgencyStokistList_ddl()
        {
            return UploadHelper.AgencyStokistList_ddl();
        }
        #endregion

        #region[Agency Credit Limit]
        internal static string AgencyList_ddl()
        {
            return UploadHelper.AgencyList_ddl();
        }
        internal static ProfileModel.Agency GetAgencyDetails(string userId)
        {
            return UploadHelper.GetAgencyDetails(userId);
        }
        internal static bool UpdateAgencyCreditLimit(string id, string creditlimit, string expirydate, string remarks)
        {
            return UploadHelper.UpdateAgencyCreditLimit(id, creditlimit, expirydate, remarks);
        }
        #endregion
    }
}