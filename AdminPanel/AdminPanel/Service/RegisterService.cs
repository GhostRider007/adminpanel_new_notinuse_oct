﻿using AdminPanel.Helper;
using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static AdminPanel.Models.RegisterModel;

namespace AdminPanel.Service
{
    public static class RegisterService
    {
        #region [Group Type Details]
        internal static bool InsertGroupTypeDetails(string agentType, string description)
        {
            return RegisterHelper.InsertGroupTypeDetails(agentType, description);
        }
        internal static List<AgentType> GroupTypeDetailsList()
        {
            return RegisterHelper.GroupTypeDetailsList();
        }
        internal static bool UpdateGroupTypeDetails(string actionType, string userType, string desc)
        {
            return RegisterHelper.UpdateGroupTypeDetails(actionType, userType, desc);
        }
        #endregion
    }
}