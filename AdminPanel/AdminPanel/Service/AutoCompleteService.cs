﻿using AdminPanel.Helper;
using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminPanel.Service
{
    public static class AutoCompleteService
    {
        public static List<AutoAgency> GetAgencyAutoSearch(string inputPara)
        {
            ExecuRegister lu = new ExecuRegister();
            bool isLogedIn = AdministrationService.IsExecutiveLogin(ref lu);
            return AutoCompleteHelper.GetAgencyAutoSearch(inputPara, lu.user_type, lu.user_id);
        }
    }
}