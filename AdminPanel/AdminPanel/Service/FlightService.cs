﻿using AdminPanel.Helper;
using AdminPanel.Models;

namespace AdminPanel.Service
{
    public static class FlightService
    {
        public static FlightModel GetTicketReport(string userType, string loginID, FlightModel model)
        {
            return FlightHelper.GetTicketReport(userType, loginID, model);
        }
    }
}