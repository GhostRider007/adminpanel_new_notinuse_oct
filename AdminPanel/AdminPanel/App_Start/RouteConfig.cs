﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AdminPanel
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(name: "Login", url: "", defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
            routes.MapRoute(name: "Dashboard", url: "dashboard", defaults: new { controller = "Dashboard", action = "Dashboard", id = UrlParameter.Optional });

            //Ledger Section
            routes.MapRoute(name: "LedgerReport", url: "account/ledger-report", defaults: new { controller = "Account", action = "LedgerList", id = UrlParameter.Optional });

            routes.MapRoute(name: "Default", url: "{controller}/{action}/{id}", defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }
    }
}
