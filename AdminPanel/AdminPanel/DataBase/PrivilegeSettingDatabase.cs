﻿using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static AdminPanel.Models.PrivilageSettingModel;

namespace AdminPanel.DataBase
{
    public class PrivilegeSettingDatabase
    {
        #region [Common : Connect to database]                
        private static SqlConnection conString = new SqlConnection(ConfigFile.DatabaseConnectionString);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }
        #endregion

        #region[Branch]
        internal static bool CreateBranch(string branchName, string userid)
        {
            try
            {
                Command = new SqlCommand("Sp_Branch_Opertion", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@Branch", branchName);
                Command.Parameters.AddWithValue("@CreatedBy", userid);
                Command.Parameters.AddWithValue("@Action", "insert");

                string result = "";
                OpenConnection(conString);
                result = Command.ExecuteScalar().ToString();
                CloseConnection(conString);

                if (result.ToLower() == "insert")
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        internal static DataTable GetBranchList()
        {
            try
            {
                Command = new SqlCommand("Sp_Branch_Opertion", conString);
                Command.Parameters.AddWithValue("@Action", "select");
                Command.CommandType = CommandType.StoredProcedure;
                Adapter = new SqlDataAdapter();

                OpenConnection(conString);
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "BranchList");
                ObjDataTable = ObjDataSet.Tables["BranchList"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return ObjDataTable;
        }
        internal static bool DeleteBranch(string id)
        {
            try
            {
                Command = new SqlCommand("Sp_Branch_Opertion", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@Branch_id", id);
                Command.Parameters.AddWithValue("@Action", "delete");

                OpenConnection(conString);
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(conString);
                if (isSuccess == 1)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        internal static bool UpdateBranch(string id, string branchName, string userid)
        {
            try
            {
                Command = new SqlCommand("Sp_Branch_Opertion", conString);

                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@Branch", branchName);
                Command.Parameters.AddWithValue("@Branch_ID", id);
                Command.Parameters.AddWithValue("@UpdateBy", userid);
                Command.Parameters.AddWithValue("@Action", "update");

                OpenConnection(conString);
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(conString);

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        #endregion

        #region[City Master]
        internal static DataTable GetStates()
        {
            try
            {
                Command = new SqlCommand("select STATE,STATEID from TBL_STATE order by STATE", conString);
                Command.CommandType = CommandType.Text;
                Adapter = new SqlDataAdapter();

                OpenConnection(conString);
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "StateList");
                ObjDataTable = ObjDataSet.Tables["StateList"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return ObjDataTable;
        }
        internal static DataTable GetCityList(string txtStateId, string txtCity)
        {
            try
            {
                string qry = "select c.COUNTER,c.CITY,s.STATE,c.CREATEDDATE from TBL_CITY c,TBL_STATE s where c.STATEID='" + txtStateId + "' and c.STATEID=s.STATEID and c.CITY like '%" + txtCity + "%' order by c.CREATEDDATE desc";
                Command = new SqlCommand(qry, conString);
                Command.CommandType = CommandType.Text;
                Adapter = new SqlDataAdapter();

                OpenConnection(conString);
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "CityList");
                ObjDataTable = ObjDataSet.Tables["CityList"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return ObjDataTable;
        }
        internal static bool AddCity(string stateId, string cityName)
        {
            try
            {
                Command = new SqlCommand("select COUNT(*) from TBL_CITY where STATEID='" + stateId + "' and CITY='" + cityName + "'", conString);
                Command.CommandType = CommandType.Text;
                OpenConnection(conString);
                int result = (int)Command.ExecuteScalar();
                CloseConnection(conString);
                if (result <= 0)
                {
                    Command = new SqlCommand("insert into TBL_CITY (CITY,STATEID) values ('" + cityName + "','" + stateId + "')", conString);
                    Command.CommandType = CommandType.Text;
                    OpenConnection(conString);
                    int rowsEffected = Command.ExecuteNonQuery();
                    CloseConnection(conString);

                    if (rowsEffected > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        internal static bool UpdateCity(string id, string cityName)
        {
            try
            {
                Command = new SqlCommand("update TBL_CITY set CITY='" + cityName + "' where COUNTER='" + id + "'", conString);
                Command.CommandType = CommandType.Text;
                OpenConnection(conString);
                int result = Command.ExecuteNonQuery();
                CloseConnection(conString);
                if (result > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {

                ex.ToString();
            }
            return false;
        }
        internal static bool DeleteCity(string id)
        {
            try
            {
                Command = new SqlCommand("delete from TBL_CITY where COUNTER='" + id + "'", conString);
                Command.CommandType = CommandType.Text;
                OpenConnection(conString);
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(conString);
                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        #endregion

        #region[Executive]
        internal static DataTable GetBranchList_forExecutive()
        {
            try
            {
                Command = new SqlCommand("Sp_Branch_Opertion", conString);
                Command.Parameters.AddWithValue("@Action", "select");
                Command.CommandType = CommandType.StoredProcedure;
                Adapter = new SqlDataAdapter();

                OpenConnection(conString);
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "BranchList");
                ObjDataTable = ObjDataSet.Tables["BranchList"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return ObjDataTable;
        }
        internal static DataTable GetRoleList_forExecutive()
        {
            try
            {
                Command = new SqlCommand("BindRoleNewExecu_PP", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Adapter = new SqlDataAdapter();

                OpenConnection(conString);
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "RoleList");
                ObjDataTable = ObjDataSet.Tables["RoleList"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return ObjDataTable;
        }
        internal static DataTable GetExecutiveList()
        {
            try
            {
                Command = new SqlCommand("SelectNewExecSp_PP", conString);
                Command.CommandType = CommandType.Text;
                Adapter = new SqlDataAdapter();

                OpenConnection(conString);
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "ExecutiveList");
                ObjDataTable = ObjDataSet.Tables["ExecutiveList"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return ObjDataTable;
        }
        internal static bool AddExecutive(Executive model)
        {
            try
            {
                Command = new SqlCommand("BindRoleNewExecutive_PP", conString);
                Command.CommandType = CommandType.StoredProcedure;

                Command.Parameters.AddWithValue("@userid", model.user_id);
                Command.Parameters.AddWithValue("@password", model.password);
                Command.Parameters.AddWithValue("@role_id", model.role_id);
                Command.Parameters.AddWithValue("@name", model.name);
                Command.Parameters.AddWithValue("@email_id", model.email_id);
                Command.Parameters.AddWithValue("@mobile_no", model.mobile_no);
                Command.Parameters.AddWithValue("@status", "1");
                Command.Parameters.AddWithValue("@Branch", model.Branch);
                string result = "";
                OpenConnection(conString);
                result = Command.ExecuteScalar().ToString();
                CloseConnection(conString);
                if (result == "Insert")
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        internal static bool DeleteExecutive(string userId)
        {
            try
            {
                Command = new SqlCommand("DeleteSp1_PP", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@user_id", userId);
                OpenConnection(conString);
                int rows = Command.ExecuteNonQuery();
                CloseConnection(conString);
                if (rows < 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        internal static bool UpdateCity(string user_id, string newPassword, string newRoleId, string newStatus)
        {
            try
            {
                Command = new SqlCommand("NewExecuUpdateSp_PP", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@user_id", user_id);
                Command.Parameters.AddWithValue("@password", newPassword);
                Command.Parameters.AddWithValue("@RoleID", newRoleId);
                Command.Parameters.AddWithValue("@status", newStatus.ToLower() == "true" ? 1 : 0);
                OpenConnection(conString);
                int rows = Convert.ToInt32(Command.ExecuteNonQuery());
                CloseConnection(conString);
                if (rows > 0) 
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        #endregion
    }
}