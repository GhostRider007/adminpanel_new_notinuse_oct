﻿using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AdminPanel.DataBase
{
    public static class AccountDatabase
    {
        #region [Common : Connect to database]                
        private static SqlConnection conString = new SqlConnection(ConfigFile.DatabaseConnectionString);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }
        #endregion
        public static DataTable GetLedgerDetails(string userType, string loginID, LedgerModel model)
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("GetLedgerDetail_PP", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("usertype", userType);
                Command.Parameters.AddWithValue("LoginID", loginID);
                Command.Parameters.AddWithValue("FormDate", !string.IsNullOrEmpty(model.FilterFromDate) ? model.FilterFromDate : "");
                Command.Parameters.AddWithValue("ToDate", !string.IsNullOrEmpty(model.FilterToDate) ? model.FilterToDate : "");
                Command.Parameters.AddWithValue("AgentId", !string.IsNullOrEmpty(model.FilterAgencyId) ? model.FilterAgencyId : "");
                Command.Parameters.AddWithValue("BookingType", !string.IsNullOrEmpty(model.FilterBookingType) ? model.FilterBookingType : "");
                Command.Parameters.AddWithValue("SearchType", !string.IsNullOrEmpty(model.FilterSearchType) ? model.FilterSearchType : "Own");
                Command.Parameters.AddWithValue("PaymentMode", !string.IsNullOrEmpty(model.FilterPaymentMode) ? model.FilterPaymentMode : "All");
                Command.Parameters.AddWithValue("TransType", !string.IsNullOrEmpty(model.FilterTransType) ? model.FilterTransType : "");

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "LedgerDetail");
                ObjDataTable = ObjDataSet.Tables["LedgerDetail"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        public static DataTable GetBookingTypeFromLedger()
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("usp_Get_Distinct_BookingTypeFromLedger_PP", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "BookingTypeFromLedger");
                ObjDataTable = ObjDataSet.Tables["BookingTypeFromLedger"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        public static DataTable GetTransTypeFromLedger()
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("usp_Get_Distinct_TransTypeFromLedger", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "TransTypeFromLedger");
                ObjDataTable = ObjDataSet.Tables["TransTypeFromLedger"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
               
    }
}

