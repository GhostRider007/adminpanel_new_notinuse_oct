﻿using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AdminPanel.DataBase
{
    public static class RegisterDatabase
    {
        #region [Common : Connect to database]                
        private static SqlConnection conString = new SqlConnection(ConfigFile.DatabaseConnectionString);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }
        #endregion

        #region [Group Type Details]
        internal static bool InsertGroupTypeDetails(string agentType, string description)
        {
            try
            {
                string msg = string.Empty;
                ObjDataTable = new DataTable();
                ObjDataTable = GroupTypeMGMT(agentType, description, "Insert", ref msg);
                if (msg == "ADDED")
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        internal static DataTable GroupTypeDetailsList()
        {
            try
            {
                string msg = string.Empty;
                ObjDataTable = new DataTable();
                ObjDataTable = GroupTypeMGMT("", "", "MultipleSelect", ref msg);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return ObjDataTable;
        }
        internal static bool UpdateGroupTypeDetails(string actionType, string userType, string desc)
        {
            try
            {
                string msg = string.Empty;
                ObjDataTable = new DataTable();

                if (actionType == "delete")
                {
                    ObjDataTable = GroupTypeMGMT(userType, "", "Delete", ref msg);
                    if (msg == "1") 
                    {
                        return true;
                    }
                }
                if (actionType == "Update")
                {
                    ObjDataTable = GroupTypeMGMT(userType, desc, "Update", ref msg);
                    if (msg == "1")
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        private static DataTable GroupTypeMGMT(string type, string desc, string cmdType, ref string msg)
        {
            try
            {
                Command = new SqlCommand("usp_agentTypeMGMT", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@UserType", type);
                Command.Parameters.AddWithValue("@desc", desc);
                Command.Parameters.AddWithValue("@cmdType", cmdType);
                Command.Parameters.Add("@msg", SqlDbType.VarChar, 500);
                Command.Parameters["@msg"].Direction = ParameterDirection.Output;

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "GroupTypeDetails");
                ObjDataTable = ObjDataSet.Tables["GroupTypeDetails"];
                msg = Command.Parameters["@msg"].Value.ToString().Trim();
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return ObjDataTable;
        }
        #endregion
    }
}