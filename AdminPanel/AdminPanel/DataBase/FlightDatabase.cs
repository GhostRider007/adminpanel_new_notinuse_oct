﻿using AdminPanel.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace AdminPanel.DataBase
{
    public static class FlightDatabase
    {
        #region [Common : Connect to database]                
        private static SqlConnection conString = new SqlConnection(ConfigFile.DatabaseConnectionString);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }
        #endregion

        public static DataTable GetTicketReport(string userType, string loginID, FlightModel model)
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("USP_GetTicketDetailForAdmin", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("usertype", userType);
                Command.Parameters.AddWithValue("LoginID", loginID);
                Command.Parameters.AddWithValue("FormDate", !string.IsNullOrEmpty(model.FilterFromDate) ? model.FilterFromDate : "");
                Command.Parameters.AddWithValue("ToDate", !string.IsNullOrEmpty(model.FilterToDate) ? model.FilterToDate : "");
                Command.Parameters.AddWithValue("OderId", !string.IsNullOrEmpty(model.FilterAgencyId) ? model.FilterAgencyId : "");
                Command.Parameters.AddWithValue("PNR", !string.IsNullOrEmpty(model.FilterPnr) ? model.FilterPnr : "");
                Command.Parameters.AddWithValue("AirlinePNR", !string.IsNullOrEmpty(model.FilterAirline) ? model.FilterAirline : "");
                Command.Parameters.AddWithValue("PaxName", !string.IsNullOrEmpty(model.FilterPaxName) ? model.FilterPaxName : "");
                Command.Parameters.AddWithValue("TicketNo", !string.IsNullOrEmpty(model.FilterOrderId) ? model.FilterOrderId : "");
                Command.Parameters.AddWithValue("AgentId", !string.IsNullOrEmpty(model.FilterAgencyId) ? model.FilterAgencyId : "");
                Command.Parameters.AddWithValue("Trip", !string.IsNullOrEmpty(model.FilterTripType) ? model.FilterTripType : "");
                Command.Parameters.AddWithValue("Status", "Ticketed");
                Command.Parameters.AddWithValue("PartnerName", "");
                Command.Parameters.AddWithValue("PaymentMode", "All");

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "TicketDetail");
                ObjDataTable = ObjDataSet.Tables["TicketDetail"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
    }
}