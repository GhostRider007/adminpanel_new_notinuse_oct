﻿using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AdminPanel.DataBase
{
    public class ProfileDatabase
    {
        #region [Common : Connect to database]                
        private static SqlConnection conString = new SqlConnection(ConfigFile.DatabaseConnectionString);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }
        #endregion

        #region[Change Password]
        internal static bool ChangePassword(string userid, string oldpassword, string newpassword)
        {
            try
            {
                Command = new SqlCommand("select * from ExecuRegister  where user_id='" + userid + "'", conString);
                Command.CommandType = CommandType.Text;

                OpenConnection(conString);
                SqlDataReader rd = Command.ExecuteReader();
                bool doesExist = false;
                while (rd.Read())
                {
                    if (oldpassword == rd["password"].ToString())
                    {
                        doesExist = true;
                    }
                }
                rd.Close();
                CloseConnection(conString);
                Command.Dispose();


                if (doesExist)
                {
                    Command = new SqlCommand("usp_changePassword_PP", conString);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.Add("@userID", SqlDbType.NVarChar, 500).Value = userid;
                    Command.Parameters.Add("@oldPassword", SqlDbType.NVarChar, 100).Value = oldpassword;
                    Command.Parameters.Add("@Password", SqlDbType.NVarChar, 500).Value = newpassword;

                    OpenConnection(conString);
                    int result = Convert.ToInt32(Command.ExecuteScalar());
                    CloseConnection(conString);
                    Command.Dispose();

                    if (result > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }

        #endregion

        #region[AgencyTDS]
        internal static DataTable AgencyList()
        {
            try
            {
                Command = new SqlCommand("SELECT Counter,Agency_Name,User_Id,Agent_Type,TDS,ExmptTDS,ExmptTdsLimit FROM agent_register", conString);
                Command.CommandType = CommandType.Text;
                Adapter = new SqlDataAdapter();
                OpenConnection(conString);
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "AgencyList");
                ObjDataTable = ObjDataSet.Tables["AgencyList"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return ObjDataTable;
        }
        internal static bool UpdateAgencyTDS(int id, string tds)
        {
            try
            {
                Command = new SqlCommand("UPDATE agent_register SET TDS=" + tds + " , ExmptTDS=0,ExmptTdsLimit=0 WHERE counter=" + id, conString);
                Command.CommandType = CommandType.Text;
                OpenConnection(conString);
                int result = Command.ExecuteNonQuery();
                CloseConnection(conString);
                if (result > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        #endregion
    }
}