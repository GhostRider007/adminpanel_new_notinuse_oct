﻿using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AdminPanel.DataBase
{
    public static class AutoCompleteDatabase
    {
        #region [Common : Connect to database]                
        private static SqlConnection conString = new SqlConnection(ConfigFile.DatabaseConnectionString);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }
        #endregion

        public static DataTable GetAgencyAutoSearch(string inputPara, string userType, string distr)
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("AgencyAutoSearch", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("param1", inputPara);
                Command.Parameters.AddWithValue("UserType", userType);
                Command.Parameters.AddWithValue("DistrId", distr);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "AgencySearch");
                ObjDataTable = ObjDataSet.Tables["AgencySearch"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    conString.Close();
                }
            }

            return ObjDataTable;
        }
    }
}