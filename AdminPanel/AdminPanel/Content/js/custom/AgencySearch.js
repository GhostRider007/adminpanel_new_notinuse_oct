﻿var SHandler;
$(document).ready(function () {
    SHandler = new SearchHelper();
    SHandler.BindEvents();
});

var SearchHelper = function () {
    this.AgencySearch = $(".agencysearch");
    this.HdnAgencyId = $(".hdnagencyid");
}

SearchHelper.prototype.BindEvents = function () {
    var h = this;

    h.AgencySearch.autocomplete({
        source: function (request, response) {
            $(".agencyspin").css("display", "block");
            h.HdnAgencyId.val(null);
            $.ajax({
                url: "/AutoComplete/FetchAgencyList",
                data: "{ 'searchinput': '" + request.term + "', maxResults: 10 }",
                dataType: "json", type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data, function (item) {
                        var result = item.Agency_Name + "(" + item.User_Id + ")";
                        var hidresult = item.User_Id;
                        return { label: result, value: result, id: hidresult }
                    }))
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            })
        },
        response: function (event, ui) {
            if (!ui.content.length) { var noResult = { value: "", label: "No agency matching your request" }; ui.content.push(noResult); }
            $(".agencyspin").css("display", "none");
        },
        autoFocus: true,
        minLength: 3,
        select: function (event, ui) {
            h.HdnAgencyId.val(ui.item.id);
            //h.AgencySearch.after("<span class='fa fa-times-circle-o clragency' style='position: absolute;cursor: pointer;right: 4px;font-size: 20px;top: -7px;color: red;' title='clear'></span>");
        }
    });

    //$(document.body).on('click', ".clragency", function (e) { h.AgencySearch.val(null); h.HdnAgencyId.val(null); $(".clragency").remove(); });
}

