﻿$("#ddlRole").change(function () {
    $("#txtRole").val($("#ddlRole option:selected").text());

});
$("#ddlBranch").change(function () {
    $("#txtBranch").val($("#ddlBranch option:selected").text());

});

function DeleteExecutive(user_id, id) {
    let isDelete = confirm("Are you sure want to delete this city ?");

    if (isDelete) {

        $.ajax({
            type: "Post",
            url: "/PrivilegeSetting/DeleteExecutive",
            data: '{userId:' + JSON.stringify(user_id) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data == true) {
                    alert("Data has been deleted successfully");
                    $("#resetBtn").click();
                    $("#" + id).remove();
                    datacount = datacount - 1;
                    if (datacount == 0) {
                        $("tbody").html('<tr style="text-align:center"><td colspan = "9" style = "color:red;">No Record Found</td></tr>');
                    }
                }
            }
        });
    }
}

let editCount = 0;
function EditExecutive(user_id, id) {

    if (editCount > 0) {
        return alert("Cannot edit multiple fields.\nClose open fields first.");
    }

    let oldPassword = $("#PasswordData_" + id).val();
    let oldRole = $("#RoleData_" + id).html();
    let oldStatus = $("#StatusData_" + id).html();

    $("#PasswordData_" + id).removeAttr("readonly").css("border", "1px solid black");
    $("#RoleData_" + id).html(roleDdl);
    $("#StatusData_" + id).html(statusDdl);

    $("#Action_" + id).html('<u style="color:red" onclick="UpdateExecutive(' + id + ',\'' + user_id + '\')">Update</u>&nbsp;&nbsp;&nbsp;&nbsp;<u style="color:red" onclick="CancelEditing(' + id + ',\'' + oldPassword + '\',\'' + oldRole + '\',\'' + oldStatus + '\',\'' + user_id + '\')">Cancel</u>');

    editCount++;
}
function CancelEditing(id, oldPassword, oldRole, oldStatus, user_id) {
    $("#PasswordData_" + id).val(oldPassword);
    $("#PasswordData_" + id).prop("readonly", true).css("border", "none");

    $("#RoleData_" + id).html(oldRole);
    $("#StatusData_" + id).html(oldStatus);

    $("#Action_" + id).html('<u style="color:red" onclick="EditExecutive(\'' + user_id + '\',' + id + ')">Edit</u>&nbsp;&nbsp;&nbsp;&nbsp;<u style="color:red" onclick="DeleteExecutive(\'' + user_id + '\',' + id + ')">Delete</u>');
    editCount--;
}
function UpdateExecutive(id, user_id) {

    if ($("#PasswordData_" + id).val() == '') {
        $("#PasswordData_" + id).css("border", "1px solid red");
        return !1;
    }
    else {
        $("#PasswordData_" + id).css("border", "none");
    }

    if ($("#ddlNewRole option:selected").val() == '') {
        $("#ddlNewRole").css("border", "1px solid red");
        return !1;
    }
    else {
        $("#ddlNewRole").css("border", "none");
    }

    if ($("#ddlNewStatus option:selected").val() == '') {
        $("#ddlNewStatus").css("border", "1px solid red");
        return !1;
    }
    else {
        $("#ddlNewStatus").css("border", "none");
    }

    let newPassword = $("#PasswordData_" + id).val();
    let newRoleId = $("#ddlNewRole option:selected").val();
    let newStatus = $("#ddlNewStatus option:selected").val();

    $.ajax({
        type: "Post",
        url: "/PrivilegeSetting/UpdateExecutive",
        data: '{user_id:' + JSON.stringify(user_id) + ',newPassword:' + JSON.stringify(newPassword) + ',newRoleId:' + JSON.stringify(newRoleId) + ',newStatus:' + JSON.stringify(newStatus) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data == true) {
                $("#updateStatus").html("Data has been updated successfully").css("color", "green");
            }
            else {
                $("#updateStatus").html("Data updation failed").css("color", "red");
            }
            setTimeout(function () {
                location.reload()
            }, 1000);
        }
    });
};