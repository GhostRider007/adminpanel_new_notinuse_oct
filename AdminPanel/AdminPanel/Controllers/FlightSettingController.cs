﻿using AdminPanel.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static AdminPanel.Models.FlightSettingModel;

namespace AdminPanel.Controllers
{
    public class FlightSettingController : Controller
    {
        // GET: FlightSetting
        public ActionResult Index()
        {
            return View();
        }

        #region[FareTypeMaster]
        public ActionResult FareTypeMaster(FareTypeMaster model)
        {
            List<FareTypeMaster> list = new List<FareTypeMaster>();           

            if (model.Id > 0) 
            {
                Models.ExecuRegister lu = ((List<Models.ExecuRegister>)HttpContext.Session["executive"])[0];
                model.UpdatedBy = lu.user_id;                
                bool isSuccess = FlightSettingService.UpdateFareTypeMaster(model);
                if (isSuccess) 
                {
                    ViewBag.UpdateMsg = "Data Updated Successfully";
                }
            }
            list = FlightSettingService.FareTypeMasterList();
            return View(list);
        }
        #endregion
    }
}