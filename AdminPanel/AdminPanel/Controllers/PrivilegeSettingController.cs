﻿using AdminPanel.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static AdminPanel.Models.PrivilageSettingModel;

namespace AdminPanel.Controllers
{
    public class PrivilegeSettingController : Controller
    {
        // GET: PrivilegeSetting
        public ActionResult Index()
        {
            return View();
        }

        #region[Branch]
        public ActionResult Branch()
        {
            List<Branch> list = new List<Branch>();
            list = PrivilegeSettingService.GetBranchList();
            return View(list);
        }
        public JsonResult CreateBranch(string branchName)
        {
            Models.ExecuRegister lu = ((List<Models.ExecuRegister>)HttpContext.Session["executive"])[0];
            string userid = lu.user_id;
            bool isSuccess = PrivilegeSettingService.CreateBranch(branchName, userid);

            return Json(isSuccess);
        }
        public JsonResult DeleteBranch(string id)
        {
            bool isSuccess = PrivilegeSettingService.DeleteBranch(id);
            return Json(isSuccess);
        }
        public JsonResult UpdateBranch(string id, string branchName)
        {
            Models.ExecuRegister lu = ((List<Models.ExecuRegister>)HttpContext.Session["executive"])[0];
            string userid = lu.user_id;
            bool isSuccess = PrivilegeSettingService.UpdateBranch(id, branchName, userid);

            return Json(isSuccess);
        }
        #endregion

        #region[City Master]
        public ActionResult CityMaster(string txtStateId, string txtCity)
        {
            List<CityMaster> list = new List<CityMaster>();
            ViewBag.StateList = PrivilegeSettingService.GetStates();
            list = PrivilegeSettingService.GetCityList(txtStateId, txtCity);
            return View(list);
        }
        public JsonResult AddCity(string stateId, string cityName)
        {
            bool isSuccess = PrivilegeSettingService.AddCity(stateId, cityName);
            return Json(isSuccess);
        }
        public JsonResult UpdateCity(string id, string cityName)
        {
            bool isSuccess = PrivilegeSettingService.UpdateCity(id, cityName);
            return Json(isSuccess);
        }
        public JsonResult DeleteCity(string id)
        {
            bool isSuccess = PrivilegeSettingService.DeleteCity(id);
            return Json(isSuccess);
        }
        #endregion

        #region[Executive]
        public ActionResult Executive()
        {
            List<Executive> list = new List<Executive>();
            ViewBag.RoleList = PrivilegeSettingService.GetRoleList_forExecutive();
            ViewBag.BranchList = PrivilegeSettingService.GetBranchList_forExecutive();
            list = PrivilegeSettingService.GetExecutiveList();
            return View(list);
        }
        [HttpPost]
        public ActionResult Executive(Executive model)
        {
            bool IsSuccess = PrivilegeSettingService.AddExecutive(model);
            ViewBag.Result = "Data already exist";
            if (IsSuccess)
            {
                ViewBag.Result = "Data Submitted Successfully";
            }
            return RedirectToAction("Executive");
        }
        public JsonResult DeleteExecutive(string userId)
        {
            bool IsSuccess = PrivilegeSettingService.DeleteExecutive(userId);
            return Json(IsSuccess);
        }
        public JsonResult UpdateExecutive(string user_id, string newPassword, string newRoleId, string newStatus)
        {
            bool IsSuccess = PrivilegeSettingService.UpdateCity(user_id, newPassword, newRoleId, newStatus);
            return Json(IsSuccess);
        }
        #endregion

        #region[Menu Detail]
        public ActionResult MenuDetail()
        {
            return View();
        }
        #endregion
    }
}