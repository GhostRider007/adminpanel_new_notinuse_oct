﻿using AdminPanel.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static AdminPanel.Models.UploadModel;

namespace AdminPanel.Controllers
{
    public class UploadController : Controller
    {
        // GET: Upload
        public ActionResult Index()
        {
            return View();
        }

        #region[Bank Details]
        public ActionResult BankDetails()
        {
            List<BankDetails> list = new List<BankDetails>();
            list = UploadServices.GetBankDetails();
            return View(list);
        }
        public JsonResult DeleteBankDetails(string id)
        {
            bool IsSuccess = UploadServices.DeleteBankDetails(id);
            return Json(IsSuccess);
        }
        public JsonResult AddBankDetails(BankDetails model)
        {
            bool IsSuccess = UploadServices.AddBankDetails(model);
            return Json(IsSuccess);
        }
        #endregion

        #region[Agency Credit And Debit]
        public ActionResult AgencyCreditAndDebit()
        {
            if (Session["AgentList"] == null)
            {
                Session["AgentList"] = UploadServices.AgencyStokistList_ddl();
            }
            ViewBag.AgentList = Session["AgentList"];
            return View();
        }
        #endregion

        #region[Agency Credit Limit]
        public ActionResult SetAgencyCreditLimit(string userId=null)
        {
            if (Session["AgentList"] == null)
            {
              //Session["AgentList"] = UploadServices.AgencyList_ddl();
                Session["AgentList"] = UploadServices.AgencyStokistList_ddl();
            }
            ViewBag.AgencyList = Session["AgentList"];

            ViewBag.AgencyDetail_display = "none";
            ViewBag.AgencyCredit_display = "none";

            Models.ProfileModel.Agency model = new Models.ProfileModel.Agency();

            if (!string.IsNullOrEmpty(userId))
            {
                model = UploadServices.GetAgencyDetails(userId);
                if (model.Counter > 0) {
                    ViewBag.AgencyDetail_display = "block";
                    ViewBag.AgencyCredit_display = "block";
                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult SetAgencyCreditLimit(string Id,string creditlimit, string expirydate, string remarks)
        {
            Models.ExecuRegister lu = ((List<Models.ExecuRegister>)HttpContext.Session["executive"])[0];
            string userid = lu.user_id;

            bool IsSuccess = UploadServices.UpdateAgencyCreditLimit(Id, creditlimit, expirydate, remarks);
            return RedirectToAction("SetAgencyCreditLimit");
        }
        #endregion
    }
}