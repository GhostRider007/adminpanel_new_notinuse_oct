﻿using AdminPanel.Models;
using AdminPanel.Service;
using System.Web.Mvc;

namespace AdminPanel.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult LedgerList(LedgerModel model)
        {
            ExecuRegister lu = new ExecuRegister();
            if (AdministrationService.IsExecutiveLogin(ref lu))
            {
                model = AccountService.GetLedgerDetails(lu.role_type, lu.user_id, model);
                model.BookingTypeList = CommonClass.CommonPopulateList(AccountService.GetBookingTypeFromLedger());
                model.TransTypeList = CommonClass.CommonPopulateList(AccountService.GetTransTypeFromLedger());
            }
            else
            {
                if (AdministrationService.LogoutExecutive())
                {
                    Response.Redirect("/");
                }
            }
            return View(model);
        }

    }
}