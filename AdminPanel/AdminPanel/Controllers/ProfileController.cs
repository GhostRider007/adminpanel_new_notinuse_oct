﻿using AdminPanel.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static AdminPanel.Models.ProfileModel;

namespace AdminPanel.Controllers
{
    public class ProfileController : Controller
    {
        // GET: Profile
        public ActionResult Index()
        {
            return View();
        }

        #region[Change Password]
        public ActionResult ChangePassword(bool IsUpdated = false)
        {
            if (IsUpdated)
            {
                ViewBag.UpdateConfirmation = "Password has been updated";
            }
            else
            {
                ViewBag.UpdateConfirmation = "";
            }
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(string oldpassword, string newpassword)
        {
            Models.ExecuRegister lu = ((List<Models.ExecuRegister>)HttpContext.Session["executive"])[0];
            string userid = lu.user_id;

            bool isChanged = ProfileService.ChangePassword(userid, oldpassword, newpassword);

            if (isChanged)
            {
                return RedirectToAction("ChangePassword", "Profile", new { IsUpdated = true });
            }

            return RedirectToAction("ChangePassword", "Profile", new { IsUpdated = false });
        }
        #endregion

        #region[AgencyTDS]
        public ActionResult AgencyTDS(string userid = null)
        {
            if (Session["AgentList"] == null)
            {
                Session["AgentList"] = ProfileService.AgencyList_ddl();
            }
            ViewBag.AgentList = Session["AgentList"];

            Agency model = new Agency();

            if (!string.IsNullOrEmpty(userid))
            {
                model = ProfileService.GetAgency(userid);
            }
            return View(model);
        }

        public ActionResult EditTDS(string userid)
        {
            Agency model = ProfileService.GetAgency(userid);
            return View(model);
        }
        [HttpPost]
        public ActionResult EditTDS(int id, string tds)
        {
            bool IsSuccess = ProfileService.UpdateAgencyTDS(id, tds);

            return RedirectToAction("AgencyTDS");

        }
        #endregion
    }
}