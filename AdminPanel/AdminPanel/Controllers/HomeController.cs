﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminPanel.Models;
using AdminPanel.Service;

namespace AdminPanel.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Menu(ExecuRegister lu)
        {
            MenuList model = new MenuList();
            if (!string.IsNullOrEmpty(lu.role_id.ToString()))
            {
                model.ExecMenuList = AdministrationService.ExecMenuList(Convert.ToInt32(lu.role_id));
            }
            return View(model);
        }
        public JsonResult ProcessBackendLogin(string username, string password)
        {
            string msg = string.Empty;
            return Json(AdministrationService.ExecutiveLogin(username, password, ref msg));
        }

        public JsonResult BackendLogOut()
        {
            return Json(AdministrationService.LogoutExecutive());
        }
    }
}