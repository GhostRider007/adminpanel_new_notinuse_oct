﻿using AdminPanel.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminPanel.Controllers
{
    public class AutoCompleteController : Controller
    {
        public JsonResult FetchAgencyList(string searchinput)
        {
            return Json(AutoCompleteService.GetAgencyAutoSearch(searchinput));
        }
    }
}