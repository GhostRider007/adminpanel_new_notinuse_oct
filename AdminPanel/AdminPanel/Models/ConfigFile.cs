﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AdminPanel.Models
{
    public static class ConfigFile
    {
        public static string DatabaseConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["dtconnection"].ConnectionString; }
        }
        public static string WebsiteName
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websitename"]); }
        }
        public static string MerchantKey
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["MerchantKey"]); }
        }
    }
}