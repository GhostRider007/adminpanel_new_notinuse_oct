﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminPanel.Models
{
    public class FlightSettingModel
    {
        public class FareTypeMaster
        {
            public int Id { get; set; }
            public string FareType { get; set; }
            public string DisplayName { get; set; }
            public string DisplayName2 { get; set; }
            public string DisplayName3 { get; set; }
            public string DisplayName4 { get; set; }
            public string Status { get; set; }
            public string CreatedDate { get; set; }
            public string CreatedBy { get; set; }
            public string UpdatedDate { get; set; }
            public string UpdatedBy { get; set; }
            public string Remark { get; set; }

        }
    }
}