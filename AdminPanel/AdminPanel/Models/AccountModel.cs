﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminPanel.Models
{
    public class LedgerModel
    {
        public string AgencyID { get; set; }
        public string UserId { get; set; }
        public string AgencyName { get; set; }
        public string Sector { get; set; }
        public string InvoiceNo { get; set; }
        public string PaxName { get; set; }
        public string PnrNo { get; set; }
        public string Aircode { get; set; }
        public string TicketNo { get; set; }
        public string PassengerName { get; set; }
        public string TicketingCarrier { get; set; }
        public string AccountID { get; set; }
        public string ExecutiveID { get; set; }
        public string Debit { get; set; }
        public string Credit { get; set; }
        public string Aval_Balance { get; set; }
        public DateTime CreatedDate { get; set; }
        public string BookingType { get; set; }
        public string Remark { get; set; }
        public string C { get; set; }
        public string D { get; set; }
        public string DueAmount { get; set; }
        public string CreditLimit { get; set; }
        public string AgentID { get; set; }
        public string TransType { get; set; }
        public List<LedgerModel> LedgerList { get; set; }
        public int TotalCount { get; set; }
        public decimal TotalDebit { get; set; }
        public decimal TotalCredit { get; set; }


        public string FilterBookingType { get; set; }
        public List<SelectListItem> BookingTypeList { get; set; }
        public string FilterTransType { get; set; }
        public List<SelectListItem> TransTypeList { get; set; }
        public string FilterFromDate { get; set; }
        public string FilterSearchType { get; set; }
        public string FilterPaymentMode { get; set; }
        public string FilterToDate { get; set; }
        public string FilterAgencyId { get; set; }
        public string FilterAgencyName { get; set; }
    }
}