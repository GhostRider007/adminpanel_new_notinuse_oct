﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminPanel.Models
{
    public class FlightModel
    {
        public string HeaderId { get; set; }
        public string OrderId { get; set; }
        public string sector { get; set; }
        public string Status { get; set; }
        public string MordifyStatus { get; set; }
        public string GdsPnr { get; set; }
        public string AirlinePnr { get; set; }
        public string VC { get; set; }
        public string Duration { get; set; }
        public string TripType { get; set; }
        public string Trip { get; set; }
        public string TourCode { get; set; }
        public string TotalBookingCost { get; set; }
        public string TotalAfterDis { get; set; }
        public string SFDis { get; set; }
        public string AdditionalMarkup { get; set; }
        public string Adult { get; set; }
        public string Child { get; set; }
        public string Infant { get; set; }
        public string AgentId { get; set; }
        public string AgencyName { get; set; }
        public string DistrId { get; set; }
        public string ExecutiveId { get; set; }
        public string PaymentType { get; set; }
        public string PgTitle { get; set; }
        public string PgFName { get; set; }
        public string PgLName { get; set; }
        public string PgMobile { get; set; }
        public string PgEmail { get; set; }
        public string Remark { get; set; }
        public string RejectedRemark { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }
        public string ResuID { get; set; }
        public string ResuCharge { get; set; }
        public string ResuServiseCharge { get; set; }
        public string ResuFareDiff { get; set; }
        public string PaxId { get; set; }
        public string ImportCharge { get; set; }
        public string YFLAG { get; set; }
        public string YCRN { get; set; }
        public string Y_CAN_FARE { get; set; }
        public string ProjectID { get; set; }
        public string BillNoCorp { get; set; }
        public string BookedBy { get; set; }
        public string IsMobile { get; set; }
        public string FareType { get; set; }
        public string ReferenceNo { get; set; }
        public string APIID { get; set; }
        public string PartnerName { get; set; }
        public string PreHoldREmark { get; set; }
        public string PreHoldUpdatedBy { get; set; }
        public string PreholdUpdateDate { get; set; }
        public string PaymentMode { get; set; }
        public string PgTid { get; set; }
        public string PgCharges { get; set; }
        public string FareRule { get; set; }
        public string ReIssueRefundStatus { get; set; }
        public string MSent { get; set; }
        public string GSTNO { get; set; }
        public string GST_Company_Name { get; set; }
        public string GST_Company_Address { get; set; }
        public string GST_PhoneNo { get; set; }
        public string GST_Email { get; set; }
        public string GSTRemark { get; set; }
        public string SearchId { get; set; }
        public string PNRId { get; set; }
        public string TicketId { get; set; }
        public string HoldCharge { get; set; }
        public string IsHoldByAgent { get; set; }
        public string HoldDateTime { get; set; }
        public string CreditNode { get; set; }
        public string DebitNode { get; set; }
        public string GSTDIFF { get; set; }
        public string GstUpdateDateTime { get; set; }
        public string BookedByStaff { get; set; }
        public string StaffId { get; set; }
        public string CouponCode { get; set; }
        public List<FlightModel> TicketReportList { get; set; }


        //public List<SelectListItem> TripTypeList { get; set; }
        //---------------------------------------------
        public int TotalCount { get; set; }
        public decimal TotalTicketAmount { get; set; }
        public decimal TotalTicketIssued { get; set; }

        public string FilterFromDate { get; set; }
        public string FilterToDate { get; set; }
        public string FilterPnr { get; set; }
        public string FilterOrderId { get; set; }
        public string FilterPaxName { get; set; }
        public string FilterAirline { get; set; }
        public string FilterAgencyId { get; set; }
        public string FilterAgencyName { get; set; }
        public string FilterTripType { get; set; }
    }
}