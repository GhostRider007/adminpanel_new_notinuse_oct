﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminPanel.Models
{
    public class ProfileModel
    {
        public class Agency
        {
            public int Counter { get; set; }
            public string Title { get; set; }
            public string Fname { get; set; }
            public string Mname { get; set; }
            public string Lname { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Country { get; set; }
            public string zipcode { get; set; }
            public string Phone { get; set; }
            public string Mobile { get; set; }
            public string Email { get; set; }
            public string Alt_Email { get; set; }
            public string Fax_no { get; set; }
            public string Agency_Name { get; set; }
            public string Website { get; set; }
            public string PanNo { get; set; }
            public string Status { get; set; }
            public string Stax_no { get; set; }
            public string Remark { get; set; }
            public string Sec_Qes { get; set; }
            public string Sec_Ans { get; set; }
            public string User_Id { get; set; }
            public string PWD { get; set; }
            public string Agent_Type { get; set; }
            public string Crd_Limit { get; set; }
            public string Crd_Trns_Date { get; set; }
            public string Distr { get; set; }
            public string ag_logo { get; set; }
            public string Agent_status { get; set; }
            public string TDS { get; set; }
            public string Online_Tkt { get; set; }
            public string SalesExecID { get; set; }
            public string ExmptTDS { get; set; }
            public string ExmptTdsLimit { get; set; }
            public string NAV_ID { get; set; }
            public string Y_ID { get; set; }
            public string IsCorp { get; set; }
            public string IsPWD { get; set; }
            public string Decode_ITZ { get; set; }
            public string MerchantKey_ITZ { get; set; }
            public string Pwd_ITZ { get; set; }
            public string LastLogin_ITZ { get; set; }
            public string ModeType_ITZ { get; set; }
            public string SvcType_ITZ { get; set; }
            public string District { get; set; }
            public string StateCode { get; set; }
            public string GSTNO { get; set; }
            public string GST_Company_Name { get; set; }
            public string GST_Company_Address { get; set; }
            public string GST_PhoneNo { get; set; }
            public string GST_Email { get; set; }
            public string Is_GST_Apply { get; set; }
            public string GSTRemark { get; set; }
            public string GST_UpdateDate { get; set; }
            public string GST_City { get; set; }
            public string GST_State { get; set; }
            public string GST_State_Code { get; set; }
            public string GST_Pincode { get; set; }
            public string AgencyId { get; set; }
            public string AgentLimit { get; set; }
            public string AgentLimitTrnsDate { get; set; }
            public string DueAmount { get; set; }
            public string DueAmtTrnsDate { get; set; }
            public string VirtualCreditLimit { get; set; }
            public string VirtualFromDate { get; set; }
            public string VirtualToDate { get; set; }
            public string VirtualCreditTrnsDate { get; set; }
            public string IsWhiteLabel { get; set; }
            public string StockistCrd_limit { get; set; }
            public string Area { get; set; }
            public string NamePanCard { get; set; }
            public string OTP { get; set; }
            public string OTPStatus { get; set; }
            public string OTPCreatedDate { get; set; }
            public string OTPLoginStatus { get; set; }
            public string PasswordChangeDate { get; set; }
            public string PasswordExpMsg { get; set; }
            public string Branch { get; set; }
            public string currency { get; set; }
            public string IsSupplier { get; set; }
            public string WhatsAppNo { get; set; }
            public string IsSupplierAdmin { get; set; }
        }

    }
}