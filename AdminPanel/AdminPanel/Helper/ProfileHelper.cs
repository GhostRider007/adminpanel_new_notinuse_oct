﻿using AdminPanel.DataBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static AdminPanel.Models.ProfileModel;

namespace AdminPanel.Helper
{
    public static class ProfileHelper
    {
        #region[Change Password]
        internal static bool ChangePassword(string userid, string oldpassword, string newpassword)
        {
            return ProfileDatabase.ChangePassword(userid, oldpassword, newpassword);
        }
        #endregion
        #region[AgencyTDS]
        internal static string AgencyList_ddl()
        {
            string result = "<option value=\"\">Select Agency</option>";
            try
            {
                DataTable dt = ProfileDatabase.AgencyList();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            result = result + "<option value=\"" + dt.Rows[i]["User_Id"].ToString() + "\">" + dt.Rows[i]["Agency_Name"].ToString() + "</option>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        internal static Agency GetAgency(string userid)
        {
            Agency model = new Agency();
            try
            {
                DataTable dt = ProfileDatabase.AgencyList();
                var rows = dt.AsEnumerable().Where(myRow => myRow.Field<string>("User_Id") == userid);

                foreach (DataRow row in rows)
                {
                    model.Counter = Convert.ToInt32(row["Counter"].ToString()) ;
                    model.Agency_Name = row["Agency_Name"].ToString();
                    model.User_Id = row["User_Id"].ToString();
                    model.Agent_Type = row["Agent_Type"].ToString();
                    model.TDS = row["TDS"].ToString();            
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return model;
        }
        internal static bool UpdateAgencyTDS(int id, string tds)
        {
            return ProfileDatabase.UpdateAgencyTDS(id, tds);
        }
        #endregion
    }
}