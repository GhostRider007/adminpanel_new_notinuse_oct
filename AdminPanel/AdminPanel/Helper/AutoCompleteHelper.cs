﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using AdminPanel.DataBase;
using AdminPanel.Models;

namespace AdminPanel.Helper
{
    public static class AutoCompleteHelper
    {
        public static List<AutoAgency> GetAgencyAutoSearch(string inputPara, string userType, string distr)
        {
            List<AutoAgency> agencyList = new List<AutoAgency>();
            try
            {
                DataTable dtAgencyAuto = AutoCompleteDatabase.GetAgencyAutoSearch(inputPara, userType, distr);
                if (dtAgencyAuto != null && dtAgencyAuto.Rows.Count > 0)
                {
                    for (int i = 0; i <= dtAgencyAuto.Rows.Count; i++)
                    {
                        AutoAgency agency = new AutoAgency();
                        agency.Agency_Name = dtAgencyAuto.Rows[i]["Agency_Name"].ToString();
                        agency.User_Id = dtAgencyAuto.Rows[i]["User_Id"].ToString();
                        agencyList.Add(agency);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return agencyList;
        }
    }
}