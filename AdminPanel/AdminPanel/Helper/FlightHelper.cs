﻿using AdminPanel.DataBase;
using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace AdminPanel.Helper
{
    public static class FlightHelper
    {
        public static FlightModel GetTicketReport(string userType, string loginID, FlightModel model)
        {
            FlightModel result = new FlightModel();

            try
            {
                DataTable dtReport = FlightDatabase.GetTicketReport(userType, loginID, model);
                if (dtReport != null && dtReport.Rows.Count > 0)
                {
                    List<FlightModel> ticketReportList = new List<FlightModel>();
                    for (int i = 0; i < dtReport.Rows.Count; i++)
                    {
                        FlightModel flight = new FlightModel();
                        flight.HeaderId = dtReport.Rows[i]["HeaderId"].ToString();
                        flight.OrderId = dtReport.Rows[i]["OrderId"].ToString();
                        flight.sector = dtReport.Rows[i]["sector"].ToString();
                        flight.Status = dtReport.Rows[i]["Status"].ToString();
                        flight.MordifyStatus = dtReport.Rows[i]["MordifyStatus"].ToString();
                        flight.GdsPnr = dtReport.Rows[i]["GdsPnr"].ToString();
                        flight.AirlinePnr = dtReport.Rows[i]["AirlinePnr"].ToString();
                        flight.VC = dtReport.Rows[i]["VC"].ToString();
                        flight.Duration = dtReport.Rows[i]["Duration"].ToString();
                        flight.TripType = dtReport.Rows[i]["TripType"].ToString();
                        flight.Trip = dtReport.Rows[i]["Trip"].ToString();
                        flight.TourCode = dtReport.Rows[i]["TourCode"].ToString();
                        flight.TotalBookingCost = dtReport.Rows[i]["TotalBookingCost"].ToString();
                        flight.TotalAfterDis = dtReport.Rows[i]["TotalAfterDis"].ToString();
                        flight.SFDis = dtReport.Rows[i]["SFDis"].ToString();
                        flight.AdditionalMarkup = dtReport.Rows[i]["AdditionalMarkup"].ToString();
                        flight.Adult = dtReport.Rows[i]["Adult"].ToString();
                        flight.Child = dtReport.Rows[i]["Child"].ToString();
                        flight.Infant = dtReport.Rows[i]["Infant"].ToString();
                        flight.AgentId = dtReport.Rows[i]["AgentId"].ToString();
                        flight.AgencyName = dtReport.Rows[i]["AgencyName"].ToString();
                        flight.DistrId = dtReport.Rows[i]["DistrId"].ToString();
                        flight.ExecutiveId = dtReport.Rows[i]["ExecutiveId"].ToString();
                        flight.PaymentType = dtReport.Rows[i]["PaymentType"].ToString();
                        flight.PgTitle = dtReport.Rows[i]["PgTitle"].ToString();
                        flight.PgFName = dtReport.Rows[i]["PgFName"].ToString();
                        flight.PgLName = dtReport.Rows[i]["PgLName"].ToString();
                        flight.PgMobile = dtReport.Rows[i]["PgMobile"].ToString();
                        flight.PgEmail = dtReport.Rows[i]["PgEmail"].ToString();
                        flight.Remark = dtReport.Rows[i]["Remark"].ToString();
                        flight.RejectedRemark = dtReport.Rows[i]["RejectedRemark"].ToString();
                        flight.CreateDate = dtReport.Rows[i]["CreateDate"].ToString();
                        flight.UpdateDate = dtReport.Rows[i]["UpdateDate"].ToString();
                        flight.ResuID = dtReport.Rows[i]["ResuID"].ToString();
                        flight.ResuCharge = dtReport.Rows[i]["ResuCharge"].ToString();
                        flight.ResuServiseCharge = dtReport.Rows[i]["ResuServiseCharge"].ToString();
                        flight.ResuFareDiff = dtReport.Rows[i]["ResuFareDiff"].ToString();
                        flight.PaxId = dtReport.Rows[i]["PaxId"].ToString();
                        flight.ImportCharge = dtReport.Rows[i]["ImportCharge"].ToString();
                        flight.YFLAG = dtReport.Rows[i]["YFLAG"].ToString();
                        flight.YCRN = dtReport.Rows[i]["YCRN"].ToString();
                        flight.Y_CAN_FARE = dtReport.Rows[i]["Y_CAN_FARE"].ToString();
                        flight.ProjectID = dtReport.Rows[i]["ProjectID"].ToString();
                        flight.BillNoCorp = dtReport.Rows[i]["BillNoCorp"].ToString();
                        flight.BookedBy = dtReport.Rows[i]["BookedBy"].ToString();
                        flight.IsMobile = dtReport.Rows[i]["IsMobile"].ToString();
                        flight.FareType = dtReport.Rows[i]["FareType"].ToString();
                        flight.ReferenceNo = dtReport.Rows[i]["ReferenceNo"].ToString();
                        flight.APIID = dtReport.Rows[i]["APIID"].ToString();
                        flight.PartnerName = dtReport.Rows[i]["PartnerName"].ToString();
                        flight.PreHoldREmark = dtReport.Rows[i]["PreHoldREmark"].ToString();
                        flight.PreHoldUpdatedBy = dtReport.Rows[i]["PreHoldUpdatedBy"].ToString();
                        flight.PreholdUpdateDate = dtReport.Rows[i]["PreholdUpdateDate"].ToString();
                        flight.PaymentMode = dtReport.Rows[i]["PaymentMode"].ToString();
                        flight.PgTid = dtReport.Rows[i]["PgTid"].ToString();
                        flight.PgCharges = dtReport.Rows[i]["PgCharges"].ToString();
                        flight.FareRule = dtReport.Rows[i]["FareRule"].ToString();
                        flight.ReIssueRefundStatus = dtReport.Rows[i]["ReIssueRefundStatus"].ToString();
                        flight.MSent = dtReport.Rows[i]["MSent"].ToString();
                        flight.GSTNO = dtReport.Rows[i]["GSTNO"].ToString();
                        flight.GST_Company_Name = dtReport.Rows[i]["GST_Company_Name"].ToString();
                        flight.GST_Company_Address = dtReport.Rows[i]["GST_Company_Address"].ToString();
                        flight.GST_PhoneNo = dtReport.Rows[i]["GST_PhoneNo"].ToString();
                        flight.GST_Email = dtReport.Rows[i]["GST_Email"].ToString();
                        flight.GSTRemark = dtReport.Rows[i]["GSTRemark"].ToString();
                        flight.SearchId = dtReport.Rows[i]["SearchId"].ToString();
                        flight.PNRId = dtReport.Rows[i]["PNRId"].ToString();
                        flight.TicketId = dtReport.Rows[i]["TicketId"].ToString();
                        flight.HoldCharge = dtReport.Rows[i]["HoldCharge"].ToString();
                        flight.IsHoldByAgent = dtReport.Rows[i]["IsHoldByAgent"].ToString();
                        flight.HoldDateTime = dtReport.Rows[i]["HoldDateTime"].ToString();
                        flight.CreditNode = dtReport.Rows[i]["CreditNode"].ToString();
                        flight.DebitNode = dtReport.Rows[i]["DebitNode"].ToString();
                        flight.GSTDIFF = dtReport.Rows[i]["GSTDIFF"].ToString();
                        flight.GstUpdateDateTime = dtReport.Rows[i]["GstUpdateDateTime"].ToString();
                        flight.BookedByStaff = dtReport.Rows[i]["BookedByStaff"].ToString();
                        flight.StaffId = dtReport.Rows[i]["StaffId"].ToString();
                        flight.CouponCode = dtReport.Rows[i]["CouponCode"].ToString();
                        ticketReportList.Add(flight);
                    }
                    result.TicketReportList = ticketReportList;
                    result.TotalCount = ticketReportList.Count;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
    }
}