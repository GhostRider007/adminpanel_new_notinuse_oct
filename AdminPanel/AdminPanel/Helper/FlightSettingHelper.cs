﻿using AdminPanel.DataBase;
using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static AdminPanel.Models.FlightSettingModel;

namespace AdminPanel.Helper
{
    public static class FlightSettingHelper
    {
        #region[FareTypeMaster]
        internal static List<FareTypeMaster> FareTypeMasterList()
        {
            List<FareTypeMaster> list = new List<FareTypeMaster>();
            try
            {
                DataTable dt = FlightSettingDatabase.FareTypeMasterList();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            FareTypeMaster model = new FareTypeMaster();
                            model.Id = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                            model.FareType = dt.Rows[i]["FareType"].ToString();
                            model.DisplayName = dt.Rows[i]["DisplayName"].ToString();
                            model.Remark = dt.Rows[i]["Remark"].ToString();
                            model.UpdatedDate = dt.Rows[i]["UpdatedDate"].ToString();
                            model.UpdatedBy = dt.Rows[i]["UpdatedBy"].ToString();
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return list;
        }

        internal static bool UpdateFareTypeMaster(FareTypeMaster model)
        {
            return FlightSettingDatabase.UpdateFareTypeMaster(model);
        }
        #endregion
    }
}