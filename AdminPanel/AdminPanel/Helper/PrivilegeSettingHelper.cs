﻿using AdminPanel.DataBase;
using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static AdminPanel.Models.PrivilageSettingModel;

namespace AdminPanel.Helper
{
    public static class PrivilegeSettingHelper
    {
        #region[Branch]
        internal static bool CreateBranch(string branchName, string userid)
        {
            return PrivilegeSettingDatabase.CreateBranch(branchName, userid);
        }
        internal static List<Branch> GetBranchList()
        {
            List<Branch> list = new List<Branch>();
            try
            {
                DataTable dt = PrivilegeSettingDatabase.GetBranchList();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Branch model = new Branch();
                            model.id = Convert.ToInt32(dt.Rows[i]["Branch_ID"].ToString());
                            model.BranchName = dt.Rows[i]["Branch"].ToString();
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return list;
        }
        internal static bool DeleteBranch(string id)
        {
            return PrivilegeSettingDatabase.DeleteBranch(id);
        }
        internal static bool UpdateBranch(string id, string branchName, string userid)
        {
            return PrivilegeSettingDatabase.UpdateBranch(id, branchName, userid);
        }

        #endregion

        #region[CityMaster]
        internal static string GetStates()
        {
            string result = "<option value=\"\">--Select State--</option>";
            try
            {
                DataTable dt = PrivilegeSettingDatabase.GetStates();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            result = result + "<option value=\"" + dt.Rows[i]["STATEID"].ToString() + "\">" + dt.Rows[i]["STATE"].ToString() + "</option>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        internal static List<CityMaster> GetExecutiveList(string txtStateId, string txtCity)
        {
            List<CityMaster> list = new List<CityMaster>();
            try
            {
                DataTable dt = PrivilegeSettingDatabase.GetCityList(txtStateId, txtCity);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            CityMaster model = new CityMaster();
                            model.id = Convert.ToInt32(dt.Rows[i]["COUNTER"].ToString());
                            model.CITY = dt.Rows[i]["CITY"].ToString();
                            model.STATE = dt.Rows[i]["STATE"].ToString();
                            model.CREATEDDATE = dt.Rows[i]["CREATEDDATE"].ToString();
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return list;
        }
        internal static bool AddCity(string stateId, string cityName)
        {
            return PrivilegeSettingDatabase.AddCity(stateId, cityName);
        }
        internal static bool UpdateCity(string id, string cityName)
        {
            return PrivilegeSettingDatabase.UpdateCity(id, cityName);
        }
        internal static List<CityMaster> GetCityList(string txtStateId, string txtCity)
        {
            List<CityMaster> list = new List<CityMaster>();
            try
            {
                DataTable dt = PrivilegeSettingDatabase.GetCityList(txtStateId, txtCity);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            CityMaster model = new CityMaster();
                            model.id = Convert.ToInt32(dt.Rows[i]["COUNTER"].ToString());
                            model.CITY = dt.Rows[i]["CITY"].ToString();
                            model.STATE = dt.Rows[i]["STATE"].ToString();
                            model.CREATEDDATE = dt.Rows[i]["CREATEDDATE"].ToString();
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return list;
        }
        internal static bool DeleteCity(string id)
        {
            return PrivilegeSettingDatabase.DeleteCity(id);
        }
        #endregion

        #region[Executive]
        internal static string GetBranchList_forExecutive()
        {
            string result = "<option value=\"\">--Select Branch--</option>";
            try
            {
                DataTable dt = PrivilegeSettingDatabase.GetBranchList_forExecutive();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            result = result + "<option value=\"" + dt.Rows[i]["Branch_ID"].ToString() + "\">" + dt.Rows[i]["Branch"].ToString() + "</option>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        internal static string GetRoleList_forExecutive()
        {
            string result = "<option value=\"\">--Select Role--</option>";
            try
            {
                DataTable dt = PrivilegeSettingDatabase.GetRoleList_forExecutive();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            result = result + "<option value=\"" + dt.Rows[i]["Role_id"].ToString() + "\">" + dt.Rows[i]["Role"].ToString() + "</option>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        internal static List<Executive> GetExecutiveList()
        {
            List<Executive> list = new List<Executive>();
            try
            {
                DataTable dt = PrivilegeSettingDatabase.GetExecutiveList();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Executive model = new Executive();
                            model.id = Convert.ToInt32(dt.Rows[i]["counter"].ToString());
                            model.user_id = dt.Rows[i]["user_id"].ToString();
                            model.email_id = dt.Rows[i]["email_id"].ToString();
                            model.password = dt.Rows[i]["password"].ToString();
                            model.role = dt.Rows[i]["Role"].ToString();
                            model.role_type = dt.Rows[i]["role_type"].ToString();
                            model.name = dt.Rows[i]["name"].ToString();
                            model.email_id = dt.Rows[i]["email_id"].ToString();
                            model.mobile_no = dt.Rows[i]["mobile_no"].ToString();
                            model.Status = dt.Rows[i]["status"].ToString().ToLower() == "true" ? "Active" : "Deactive";
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return list;
        }
        internal static bool AddExecutive(Executive model)
        {
            return PrivilegeSettingDatabase.AddExecutive(model);
        }
        internal static bool DeleteExecutive(string userId)
        {
            return PrivilegeSettingDatabase.DeleteExecutive(userId);
        }
        internal static bool UpdateCity(string user_id, string newPassword, string newRoleId, string newStatus)
        {
            return PrivilegeSettingDatabase.UpdateCity(user_id, newPassword, newRoleId, newStatus);
        }
        #endregion
    }
}