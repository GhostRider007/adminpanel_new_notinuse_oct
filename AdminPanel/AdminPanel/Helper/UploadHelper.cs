﻿using AdminPanel.DataBase;
using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static AdminPanel.Models.UploadModel;

namespace AdminPanel.Helper
{
    public class UploadHelper
    {
        #region[Bank Details]
        internal static List<BankDetails> GetBankDetails()
        {
            List<BankDetails> list = new List<BankDetails>();
            try
            {
                DataTable dt = UploadDatabase.GetBankDetails();

                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        BankDetails model = new BankDetails();
                        model.id = Convert.ToInt32(dt.Rows[i]["Counter"].ToString());
                        model.BankName = dt.Rows[i]["BankName"].ToString();
                        model.BranchName = dt.Rows[i]["BranchName"].ToString();
                        model.Area = dt.Rows[i]["Area"].ToString();
                        model.AccountNumber = dt.Rows[i]["AccountNumber"].ToString();
                        model.NEFTCode = dt.Rows[i]["NEFTCode"].ToString();
                        //model.CreatedDate = dt.Rows[i]["CreatedDate"].ToString();
                        model.DISTRID = dt.Rows[i]["DISTRID"].ToString();
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return list;
        }
        internal static bool DeleteBankDetails(string id)
        {
            try
            {
                bool isSuccess = UploadDatabase.DeleteBankDetails(id);
                if (isSuccess)
                {
                    return isSuccess;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        internal static bool AddBankDetails(BankDetails model)
        {
            try
            {
                bool isSuccess = UploadDatabase.AddBankDetails(model);
                if (isSuccess)
                {
                    return isSuccess;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        #endregion

        #region[Agency Credit And Debit]
        internal static string AgencyStokistList_ddl()
        {
            string result = "<option value=\"\">Select Agency</option>";
            try
            {
                DataTable dt = UploadDatabase.AgencyStokistList_ddl();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            result = result + "<option value=\"" + dt.Rows[i]["User_Id"].ToString() + "\" data-agencyid=\"" + dt.Rows[i]["AgencyId"].ToString() + "\" data-agencytype=\"" + dt.Rows[i]["Agent_Type"].ToString() + "\">" + dt.Rows[i]["Agency_Name"].ToString() + " (" + dt.Rows[i]["User_Id"].ToString() + ", " + dt.Rows[i]["AgencyId"].ToString() + ")</option>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        #endregion

        #region[Agency Credit Limit]
        internal static string AgencyList_ddl()
        {
            string result = "<option value=\"\">Select Agency</option>";
            try
            {
                DataTable dt = UploadDatabase.AgencyList();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            result = result + "<option value=\"" + dt.Rows[i]["User_Id"].ToString() + "\">" + dt.Rows[i]["Agency_Name"].ToString() + "</option>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        internal static ProfileModel.Agency GetAgencyDetails(string userId)
        {
            ProfileModel.Agency model = new ProfileModel.Agency();
            try
            {
                DataTable dt = UploadDatabase.AgencyDetail(userId);

                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        model.Counter = Convert.ToInt32(dt.Rows[i]["Counter"].ToString());
                        model.Agency_Name = dt.Rows[i]["Agency_Name"].ToString();
                        model.AgencyId = dt.Rows[i]["AgencyId"].ToString();
                        model.Mobile = dt.Rows[i]["Mobile"].ToString();
                        model.Crd_Limit = dt.Rows[i]["Crd_Limit"].ToString();
                        model.Title = dt.Rows[i]["Title"].ToString();
                        model.Fname = dt.Rows[i]["Fname"].ToString();
                        model.Mname = dt.Rows[i]["Mname"].ToString();
                        model.Lname = dt.Rows[i]["Lname"].ToString();
                        model.Email = dt.Rows[i]["Email"].ToString();
                        model.DueAmount = dt.Rows[i]["DueAmount"].ToString();
                        model.Address = dt.Rows[i]["Address"].ToString();
                        model.Remark = dt.Rows[i]["Remark"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return model;
        }
        internal static bool UpdateAgencyCreditLimit(string id, string creditlimit, string expirydate, string remarks)
        {
            return UploadDatabase.UpdateAgencyCreditLimit(id, creditlimit, expirydate, remarks);
        }
        #endregion
    }
}