﻿using AdminPanel.DataBase;
using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AdminPanel.Helper
{
    public static class AccountHelper
    {
        public static LedgerModel GetLedgerDetails(string userType, string loginID, LedgerModel model)
        {
            LedgerModel result = new LedgerModel();
            
            try
            {
                DataTable dtLedger = AccountDatabase.GetLedgerDetails(userType, loginID, model);
                if (dtLedger != null && dtLedger.Rows.Count > 0)
                {
                    decimal tempCredit = 0;
                    decimal tempDebit = 0;
                    List<LedgerModel> ledList = new List<LedgerModel>();
                    for (int i = 0; i < dtLedger.Rows.Count; i++)
                    {
                        LedgerModel ledger = new LedgerModel();
                        ledger.AgencyID = dtLedger.Rows[i]["AgencyID"].ToString();
                        ledger.UserId = dtLedger.Rows[i]["UserId"].ToString();
                        ledger.AgencyName = dtLedger.Rows[i]["AgencyName"].ToString();
                        ledger.Sector = dtLedger.Rows[i]["Sector"].ToString();
                        ledger.InvoiceNo = dtLedger.Rows[i]["InvoiceNo"].ToString();
                        ledger.PaxName = dtLedger.Rows[i]["PaxName"].ToString();
                        ledger.PnrNo = dtLedger.Rows[i]["PnrNo"].ToString();
                        ledger.Aircode = dtLedger.Rows[i]["Aircode"].ToString();
                        ledger.TicketNo = dtLedger.Rows[i]["TicketNo"].ToString();
                        ledger.PassengerName = dtLedger.Rows[i]["PassengerName"].ToString();
                        ledger.TicketingCarrier = dtLedger.Rows[i]["TicketingCarrier"].ToString();
                        ledger.AccountID = dtLedger.Rows[i]["AccountID"].ToString();
                        ledger.ExecutiveID = dtLedger.Rows[i]["ExecutiveID"].ToString();
                        ledger.Debit = dtLedger.Rows[i]["Debit"].ToString();
                        tempDebit = tempDebit + Convert.ToDecimal(ledger.Debit);
                        ledger.Credit = dtLedger.Rows[i]["Credit"].ToString();
                        tempCredit = tempCredit + Convert.ToDecimal(ledger.Credit);
                        ledger.Aval_Balance = dtLedger.Rows[i]["Aval_Balance"].ToString();
                        ledger.CreatedDate = Convert.ToDateTime(dtLedger.Rows[i]["CreatedDate"].ToString());
                        ledger.BookingType = dtLedger.Rows[i]["BookingType"].ToString();
                        ledger.Remark = dtLedger.Rows[i]["Remark"].ToString();
                        ledger.C = dtLedger.Rows[i]["C"].ToString();
                        ledger.D = dtLedger.Rows[i]["D"].ToString();
                        ledger.DueAmount = dtLedger.Rows[i]["DueAmount"].ToString();
                        ledger.CreditLimit = dtLedger.Rows[i]["CreditLimit"].ToString();
                        ledger.AgentID = dtLedger.Rows[i]["AgentID"].ToString();
                        ledger.TransType = dtLedger.Rows[i]["TransType"].ToString();
                        ledList.Add(ledger);
                    }
                    result.LedgerList = ledList;
                    result.TotalCount = ledList.Count;
                    result.TotalDebit = tempDebit;
                    result.TotalCredit = tempCredit;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static Dictionary<string, string> GetBookingTypeFromLedger()
        {
            Dictionary<string, string> pairList = new Dictionary<string, string>();
            try
            {
                DataTable dtBookingType = AccountDatabase.GetBookingTypeFromLedger();
                if (dtBookingType != null && dtBookingType.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBookingType.Rows.Count; i++)
                    {
                        pairList.Add(dtBookingType.Rows[i]["BookingType"].ToString(), dtBookingType.Rows[i]["BookingType"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return pairList;
        }
        public static Dictionary<string, string> GetTransTypeFromLedger()
        {
            Dictionary<string, string> pairList = new Dictionary<string, string>();
            try
            {
                DataTable dtTransType = AccountDatabase.GetTransTypeFromLedger();
                if (dtTransType != null && dtTransType.Rows.Count > 0)
                {
                    for (int i = 0; i < dtTransType.Rows.Count; i++)
                    {
                        pairList.Add(dtTransType.Rows[i]["TransType"].ToString(), dtTransType.Rows[i]["TransType"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return pairList;
        }
    }
}