﻿using AdminPanel.DataBase;
using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static AdminPanel.Models.RegisterModel;

namespace AdminPanel.Helper
{
    public static class RegisterHelper
    {
        #region [Group Type Details]
        internal static bool InsertGroupTypeDetails(string agentType, string description)
        {
            return RegisterDatabase.InsertGroupTypeDetails(agentType, description);
        }
        internal static List<AgentType> GroupTypeDetailsList()
        {
            List<AgentType> list = new List<AgentType>();
            try
            {
                DataTable dt = RegisterDatabase.GroupTypeDetailsList();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            AgentType model = new AgentType();
                            model.Counter = Convert.ToInt32(dt.Rows[i]["Counter"].ToString());
                            model.GroupType = dt.Rows[i]["GroupType"].ToString();
                            model.Text = dt.Rows[i]["Text"].ToString();
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return list;
        }
        internal static bool UpdateGroupTypeDetails(string actionType, string userType, string desc)
        {
            return RegisterDatabase.UpdateGroupTypeDetails(actionType, userType, desc);
        }
        #endregion
    }
}